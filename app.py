from pprint import pprint
from textwrap import wrap
from flask import Flask, jsonify, request
import json
import messagebird
from messagebird.conversation_message import MESSAGE_TYPE_TEXT, MESSAGE_TYPE_IMAGE, MESSAGE_TYPE_AUDIO
conversation_id = "7a31743319464ecaae1fda9ebcf998c1"
access_key = "BhQvN7z3IBrKnSeZ06LX0unHn"
channel_id = "aac492ad8c00487084800317c09e9ce0"
import re
import os
import requests

app = Flask(__name__)
methods = ["GET", "POST", "PATCH", "DELETE"]


@app.route("/", methods=methods, defaults={"path": ""})
@app.route("/<path:path>", methods=methods)
def hello_world(path):
    divider = "================================================================"
    j = request.get_json()

    print(divider)
    print(f"*** Received data at: {path}")

    print("\n** data:")
    print("\n".join(wrap(request.data.decode())))

    print("\n** form:")
    pprint(request.form)

    print("\n** json:")
    pprint(j)
    conversation_id = j['conversation']['id']
    print(conversation_id)
    message_received = j['message']['content']['text']
    print(message_received)

    total = 0 #sintaxis mensaje suma nro1 nro2
    if re.match("suma", message_received):
        args = message_received.split(" ")
        total = int(args[1]) + int(args[2])

        client = messagebird.Client(access_key)
        msg = client.conversation_create_message(conversation_id,
                                                 {'channelId': channel_id, 'type': MESSAGE_TYPE_TEXT,
                                                  'content': {'text': "Su suma da: %d"%total}})


    elif (re.match("mapamundi", message_received)):
        client = messagebird.Client(access_key)
        msg = client.conversation_create_message(conversation_id,
                                                 {'channelId': channel_id, 'type': MESSAGE_TYPE_IMAGE,
                                                  'content': {'image': {"url": "https://upload.wikimedia.org/wikipedia/commons/9/91/Winkel_triple_projection_SW.jpg"}}})

    elif (re.match("alarms", message_received)):
        auth_data = requests.post('http://localhost:8020/api/token/',
                      {
                          'username': 'admin',
                          'password': 'MyaVNe2u'
                      }
                    )
        access_token = auth_data.json()['access']

        active_alarms = requests.get('http://localhost:8020/delayWebAdmin/message_api/get_active_alarms',
                                     headers={'Authorization': 'Bearer %s' % access_token})




        client = messagebird.Client(access_key)

        print(active_alarms.json())
        """
        'node_type': alarm.node_type,
        'central': alarm.central,
        'olt': alarm.olt,
        'prefix': alarm.prefix,
        'insertion_time': alarm.insertion_time
        """

        for alarm in active_alarms.json()['alarms']:
            print(alarm)
            msg = client.conversation_create_message(conversation_id,
                                                     {
                                                         'channelId': channel_id,
                                                         'type': "text",
                                                         'content': {
                                                             'text': "Alarma en la central *%s*, iniciada el %s" % (alarm['central'], alarm['insertion_time'])
                                                         }
                                                     }
                                                     )

    # elif len(message_received) >= 1:
    #     client = messagebird.Client(access_key)
    #     msg = client.conversation_create_message(conversation_id,
    #                                              {'channelId': channel_id, 'type': MESSAGE_TYPE_TEXT,
    #                                               'content': {'text': "Comando no encontrado, las opciones disponibles son *suma* y *mapamundi*"}})

    print(f"{divider}\n\n")
    print(len(message_received))

    return jsonify(
        {
            "endpoint": path,
            "data": request.data.decode("utf-8"),
            "form": request.form,
            "json": request.get_json(),
        }
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)


"""Leer!

curl -X "POST" "https://conversations.messagebird.com/v1/webhooks" \
-H "Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn" \
-H "Content-Type: application/json" \
--data '{
"events":["message.created", "message.updated"],
"channelId": "aac492ad8c00487084800317c09e9ce0",
"url":"https://1afc-2800-a4-3019-5200-4bd2-f555-f2fe-dbcb.ngrok.io/"
}'

el id que voy a recibir es el conversation id!

ver historial mensajes

curl "https://conversations.messagebird.com/v1/conversations/195171b98b684c9c914301b19c08d01c/messages" \
-H "Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn"

https://conversations.messagebird.com/v1/conversations/<conversationID>/messages
responder mensaje

curl -X POST \
  https://conversations.messagebird.com/v1/conversations/776ef4fead40452686535582a25c74b2/messages \
  -H 'Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn' \
  -H 'Content-Type: application/json' \
  -d '{
	"content": {
		"text": "respuesta automatica"
	},
	"type": "text"
}'

obtener accesskey https://dashboard.messagebird.com/en/developers/access , es la live key
obtener channel id https://dashboard.messagebird.com/en/whatsapp/sandbox
exposicion a internet con ngok https://dashboard.ngrok.com/get-started/setup

#el channel id cuando tengamos la version definitiva va a ser fijo, asi que lo vamos a poder tener en los ENVS para que el settings lo agarre y luego lo importamos

"""
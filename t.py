#!/usr/bin/env python


#access key BhQvN7z3IBrKnSeZ06LX0unHn

import argparse
import messagebird
from messagebird.conversation_webhook import \
    CONVERSATION_WEBHOOK_EVENT_CONVERSATION_CREATED, \
    CONVERSATION_WEBHOOK_EVENT_CONVERSATION_UPDATED

parser = argparse.ArgumentParser()
parser.add_argument('--accessKey', help='access key for MessageBird API', type=str, required=True)
parser.add_argument('--channelId', help='channel that you want create the webhook', type=str, required=True)
parser.add_argument('--url', help='url for the webhook', type=str)
args = vars(parser.parse_args())

try:
    client = messagebird.Client(args['accessKey'])

    print(args['channelId'])
    webhook = client.conversation_create_webhook({
        'channelId': args['channelId'],
        'events': [CONVERSATION_WEBHOOK_EVENT_CONVERSATION_CREATED, CONVERSATION_WEBHOOK_EVENT_CONVERSATION_UPDATED],
        'url': "http://1afc-2800-a4-3019-5200-4bd2-f555-f2fe-dbcb.ngrok.io/"
    })

    # Print the object information.
    print('The following information was returned as a Webhook object:')
    print(webhook)

except messagebird.client.ErrorException as e:
    print('An error occured while requesting a Webhook object:')

    for error in e.errors:
        print('  code        : %d' % error.code)
        print('  description : %s' % error.description)
        print('  parameter   : %s\n' % error.parameter)


"""

curl -X "POST" "https://conversations.messagebird.com/v1/webhooks" \
-H "Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn" \
-H "Content-Type: application/json" \
--data '{
"events":["message.created", "message.updated"],
"channelId": "aac492ad8c00487084800317c09e9ce0",
"url":"https://1afc-2800-a4-3019-5200-4bd2-f555-f2fe-dbcb.ngrok.io/"
}'

el id que voy a recibir es el conversation id!

ver historial mensajes

curl "https://conversations.messagebird.com/v1/conversations/195171b98b684c9c914301b19c08d01c/messages" \
-H "Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn"

https://conversations.messagebird.com/v1/conversations/<conversationID>/messages
responder mensaje

curl -X POST \
  https://conversations.messagebird.com/v1/conversations/776ef4fead40452686535582a25c74b2/messages \
  -H 'Authorization: AccessKey BhQvN7z3IBrKnSeZ06LX0unHn' \
  -H 'Content-Type: application/json' \
  -d '{
	"content": {
		"text": "respuesta automatica"
	},
	"type": "text"
}'

"""